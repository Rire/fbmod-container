#!/bin/bash
PRG="$0"


echo -e "\033[38;5;202m
README
------
The update-filebot.sh tool patches core application files with the latest development revision, and does \033[4mnot\033[24m upgrade the entire application bundle.
The latest revision may or may not be compatible with the current stable application bundle, and may require you to upgrade to upgrade to the latest application bundle once in a blue moon.

Please report regression issues in the FileBot Forums:
https://www.filebot.net/report.html
\033[0m"


# resolve relative symlinks
while [ -h "$PRG" ]; do
	ls=`ls -ld "$PRG"`
	link=`expr "$ls" : '.*-> \(.*\)$'`
	if expr "$link" : '/.*' > /dev/null; then
		PRG="$link"
	else
		PRG="`dirname "$PRG"`/$link"
	fi
done

# make it fully qualified
PRG_DIR=`dirname "$PRG"`
FILEBOT_HOME=`cd "$PRG_DIR" && pwd`


# update core application files
PACKAGE_NAME="CHANGES.tar.xz"
PACKAGE_FILE="$FILEBOT_HOME/data/$PACKAGE_NAME"
PACKAGE_URL="https://get.filebot.net/filebot/HEAD/$PACKAGE_NAME"

SIGNATURE_FILE="$PACKAGE_FILE.asc"
SIGNATURE_URL="$PACKAGE_URL.asc"


# use *.asc file to check for updates
echo "Update $PACKAGE_FILE"
HTTP_CODE=`curl -L -o "$SIGNATURE_FILE" -z "$SIGNATURE_FILE" --retry 5 "$SIGNATURE_URL" -w "%{http_code}"`

if [ $HTTP_CODE -ne 200 ]; then
	echo "$HTTP_CODE NO UPDATE"
	exit 1
fi

curl -L -o "$PACKAGE_FILE" -z "$PACKAGE_FILE" --retry 5 "$PACKAGE_URL"


# verify signature and extract tar
if gpgv --homedir "$FILEBOT_HOME" --keyring "maintainer.gpg" "$SIGNATURE_FILE" "$PACKAGE_FILE"; then
	tar -C "$FILEBOT_HOME" -xvf "$PACKAGE_FILE"
fi

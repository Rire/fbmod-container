FROM debian:bullseye-slim

WORKDIR  /env

RUN for i in 1 2 3 4 5 6 7 8; do mkdir -p "/usr/share/man/man$i"; done && apt update && apt -y install mkvtoolnix mediainfo openjdk-17-jre-headless locales curl && curl -sL https://deb.nodesource.com/setup_lts.x   | bash - && \
    apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y locales nodejs &&\
        sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
            dpkg-reconfigure --frontend=noninteractive locales && \
                update-locale LANG=en_US.UTF-8

ENV LANG en_US.UTF-8 

#######################
#Specify your UID here#
#######################

ARG USERID=1000

ARG GROUPID=1000

RUN groupadd -g ${GROUPID} watcher && useradd -l -u ${USERID} -g watcher watcher

#COPY --chown=watcher:watcher . /env

COPY . /env

RUN chown -Rv watcher:watcher /env

RUN npm install

#USER watcher

cmd node watcher.js



const chokidar = require('chokidar')
const execSync = require("child_process").execSync;
var Queue = require('better-queue');
console.log("Watching...");
//your folders you want watched here
const files = ["/opt/mybook/public", "!chunk*"]
//filebot path
const filebotpath = 'filebot/filebot.sh'
//move | copy | keeplink | symlink | hardlink | test
var action = "hardlink"
//self-explanatory
var outputfolder = "/opt/mybook/link"

var q = new Queue(async function (task, cb) {
	const result1 = await execSync(`${filebotpath} -script fn:amc --def seriesDB=TheMovieDB::TV animeDB=TheMovieDB::TV --def minFileSize=0  -non-strict --action "${action}" "${task}" --output "${outputfolder}"`);
	console.log(result1.toString("utf8"))
	cb();
  }, {concurrent: 2, afterProcessDelay: 300})

const watcher = chokidar.watch(files, 
{
    ignoreInitial: true,
    usePolling: false,
    awaitWriteFinish: 
    { 
            stabilityThreshold: 2000,
            pollInterval: 100
    }
});

watcher.on('add', (path, stats) => {
    if(path.endsWith('.mkv') || path.endsWith('.mp4') || path.endsWith('.avi') || path.endsWith('.webm'))
    {
        console.log(`Processing ${path} ...`)
        q.push(path)
    }
});


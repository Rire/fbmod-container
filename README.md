# fbmod-container

A Dockerized Filebot Watcher

*  Supports watching multiple directories
*  Supports manually running a full scan over multiple directories

# Usage

Install `docker`, and optionally `docker-compose`.

Git clone this repository.

`# docker build`


There are several comments I've left in my script that will show you tweakable values, which you will more than likely have to tweak for your setup.
USERID and GROUPID may need to be tweaked too inside the Dockerfile if your USERID isn't 1000.

This is required, because filebot spits out files that are owned by root, otherwise.

I've also included a sample docker-compose.yaml file that is optional, too. Which will likely need tweaking.

If you run into any ENOSPC errors, exec `$ echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p`

Feel free to edit the command string too, since filebot supports a multitude of formatting options for renaming.

